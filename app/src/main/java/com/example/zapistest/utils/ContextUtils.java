package com.example.zapistest.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class ContextUtils {
    public static int dp2pixels(Context context, float dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics));
    }

    public static int pixels2dp(Context context, float px) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) dp;
    }
}