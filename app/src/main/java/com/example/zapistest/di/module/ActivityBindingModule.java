package com.example.zapistest.di.module;

import com.example.zapistest.ui.details.DetailsActivity;
import com.example.zapistest.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = FragmentBindingModule.class)
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector
    abstract DetailsActivity bindDetailsActivity();
}
