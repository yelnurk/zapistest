package com.example.zapistest.di.module;

import com.example.zapistest.ui.home.HomeFragment;
import com.example.zapistest.ui.profile.ProfileFragment;
import com.example.zapistest.ui.search.SearchFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBindingModule {
    @ContributesAndroidInjector
    abstract HomeFragment bindHomeFragment();

    @ContributesAndroidInjector
    abstract SearchFragment bindSearchFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment bindProfileFragment();
}
