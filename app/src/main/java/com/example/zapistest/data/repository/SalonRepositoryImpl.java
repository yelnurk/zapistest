package com.example.zapistest.data.repository;

import com.example.zapistest.data.model.SalonDetailsResponse;
import com.example.zapistest.data.model.SalonResponse;
import com.example.zapistest.data.net.SalonService;
import com.example.zapistest.domain.entity.Salon;
import com.example.zapistest.domain.repository.SalonRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SalonRepositoryImpl implements SalonRepository {
    private final SalonService salonService;

    @Inject
    public SalonRepositoryImpl(SalonService salonService) {
        this.salonService = salonService;
    }

    @Override
    public Single<List<Salon>> getPopular() {
        return salonService.getPopular()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(SalonResponse::getSalonList);
    }

    @Override
    public Single<List<Salon>> getRecommended() {
        return salonService.getRecommended()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(SalonResponse::getSalonList);
    }

    @Override
    public Single<List<Salon>> getRecentlyAdded() {
        return salonService.getRecentlyAdded()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(SalonResponse::getSalonList);
    }

    @Override
    public Single<SalonDetailsResponse> getSalonDetails(int id) {
        return salonService.getSalonDetails(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
