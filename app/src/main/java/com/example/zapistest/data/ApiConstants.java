package com.example.zapistest.data;

import com.example.zapistest.BuildConfig;

public class ApiConstants {
    private static final String SCHEME = BuildConfig.BASE_SCHEME;
    private static final String BASE_HOST = BuildConfig.BASE_HOST;
    private static final String BASE_PATH = BuildConfig.BASE_PATH;
    public static final String HOST_URL = SCHEME + BASE_HOST;
    public static final String API_BASE_URL = HOST_URL + BASE_PATH;
}
