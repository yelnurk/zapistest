package com.example.zapistest.data.model;

import com.example.zapistest.domain.entity.SalonDetails;
import com.google.gson.annotations.SerializedName;

public class SalonDetailsResponse {
    @SerializedName("salon")
    private SalonDetails salon;

    public SalonDetails getSalon() {
        return salon;
    }
}
