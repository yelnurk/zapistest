package com.example.zapistest.data.model;

import com.example.zapistest.domain.entity.Salon;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalonResponse {
    @SerializedName("salons")
    private List<Salon> salonList;

    public List<Salon> getSalonList() {
        return salonList;
    }
}
