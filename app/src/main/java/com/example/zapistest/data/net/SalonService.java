package com.example.zapistest.data.net;

import com.example.zapistest.data.model.SalonDetailsResponse;
import com.example.zapistest.data.model.SalonResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SalonService {

    @GET("salon/getSalonDetails")
    Single<SalonResponse> getPopular();

    @GET("salon/getRecommended")
    Single<SalonResponse> getRecommended();

    @GET("salon/getRecentlyAdded")
    Single<SalonResponse> getRecentlyAdded();

    @GET("salon/page")
    Single<SalonDetailsResponse> getSalonDetails(@Query("id") int id);
}