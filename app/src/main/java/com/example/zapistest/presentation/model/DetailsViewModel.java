package com.example.zapistest.presentation.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.zapistest.data.model.SalonDetailsResponse;
import com.example.zapistest.data.repository.SalonRepositoryImpl;
import com.example.zapistest.domain.entity.SalonDetails;
import com.example.zapistest.domain.repository.SalonRepository;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class DetailsViewModel extends ViewModel {

    private final SalonRepository salonRepository;
    private CompositeDisposable disposable;

    private final MutableLiveData<SalonDetails> salonDetails = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    @Inject
    public DetailsViewModel(SalonRepositoryImpl salonRepository) {
        this.salonRepository = salonRepository;
        disposable = new CompositeDisposable();
    }

    public LiveData<SalonDetails> getSalonDetails() {
        return salonDetails;
    }

    public LiveData<Boolean> getError() {
        return repoLoadError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    public void fetchDetails(int id) {
        loading.setValue(true);
        disposable.add(salonRepository.getSalonDetails(id).subscribeWith(
                new DisposableSingleObserver<SalonDetailsResponse>() {
                    @Override
                    public void onSuccess(SalonDetailsResponse salonDetailsResponse) {
                        repoLoadError.setValue(false);
                        salonDetails.setValue(salonDetailsResponse.getSalon());
                        loading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                        Timber.e(e);
                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}