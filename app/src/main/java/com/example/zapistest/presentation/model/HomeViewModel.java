package com.example.zapistest.presentation.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.zapistest.data.repository.SalonRepositoryImpl;
import com.example.zapistest.domain.entity.Salon;
import com.example.zapistest.domain.repository.SalonRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class HomeViewModel extends ViewModel {

    private final SalonRepository salonRepository;
    private CompositeDisposable disposable;

    private final MutableLiveData<List<Salon>> popular = new MutableLiveData<>();
    private final MutableLiveData<List<Salon>> recommended = new MutableLiveData<>();
    private final MutableLiveData<List<Salon>> recentlyAdded = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    @Inject
    public HomeViewModel(SalonRepositoryImpl salonRepository) {
        this.salonRepository = salonRepository;
        disposable = new CompositeDisposable();
        fetchData();
    }

    public LiveData<List<Salon>> getPopular() {
        return popular;
    }

    public LiveData<List<Salon>> getRecommended() {
        return recommended;
    }

    public LiveData<List<Salon>> getRecentlyAdded() {
        return recentlyAdded;
    }

    public LiveData<Boolean> getError() {
        return repoLoadError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    private void fetchData() {
        fetchPopular();
        fetchRecommended();
        fetchRecentlyAdded();
    }

    private void fetchRecommended() {
        loading.setValue(true);
        disposable.add(salonRepository.getRecommended().subscribeWith(
                new DisposableSingleObserver<List<Salon>>() {
                    @Override
                    public void onSuccess(List<Salon> salons) {
                        repoLoadError.setValue(false);
                        recommended.setValue(salons);
                        loading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                        Timber.e(e);
                    }
                }));
    }

    private void fetchPopular() {
        loading.setValue(true);
        disposable.add(salonRepository.getPopular().subscribeWith(
                new DisposableSingleObserver<List<Salon>>() {
                    @Override
                    public void onSuccess(List<Salon> salons) {
                        repoLoadError.setValue(false);
                        popular.setValue(salons);
                        loading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                        Timber.e(e);
                    }
                }));
    }

    private void fetchRecentlyAdded() {
        loading.setValue(true);
        disposable.add(salonRepository.getRecentlyAdded().subscribeWith(
                new DisposableSingleObserver<List<Salon>>() {
                    @Override
                    public void onSuccess(List<Salon> salons) {
                        repoLoadError.setValue(false);
                        recentlyAdded.setValue(salons);
                        loading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                        Timber.e(e);
                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}