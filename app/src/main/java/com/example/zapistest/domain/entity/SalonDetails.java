package com.example.zapistest.domain.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SalonDetails implements Serializable {
    @SerializedName("id")
    private Integer id;
    @SerializedName("category")
    private String category;
    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("workStartTime")
    private String workStartTime;
    @SerializedName("workEndTime")
    private String workEndTime;
    @SerializedName("description")
    private String description;
    @SerializedName("type")
    private String type;
    @SerializedName("checkRating")
    private Double checkRating;
    @SerializedName("instagramProfile")
    private String instagramProfile;
    @SerializedName("isMastersHidden")
    private Boolean isMastersHidden;
    @SerializedName("avatarUrl")
    private String avatarUrl;
    @SerializedName("reviewCount")
    private Integer reviewCount;
    @SerializedName("averageRating")
    private Double averageRating;
    @SerializedName("phoneNumbers")
    private List<String> phoneNumbers = null;
    @SerializedName("todayReservationsCount")
    private Object todayReservationsCount;
    @SerializedName("pictures")
    private List<String> pictures = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkStartTime() {
        return workStartTime;
    }

    public void setWorkStartTime(String workStartTime) {
        this.workStartTime = workStartTime;
    }

    public String getWorkEndTime() {
        return workEndTime;
    }

    public void setWorkEndTime(String workEndTime) {
        this.workEndTime = workEndTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getCheckRating() {
        return checkRating;
    }

    public void setCheckRating(Double checkRating) {
        this.checkRating = checkRating;
    }

    public String getInstagramProfile() {
        return instagramProfile;
    }

    public void setInstagramProfile(String instagramProfile) {
        this.instagramProfile = instagramProfile;
    }

    public Boolean getMastersHidden() {
        return isMastersHidden;
    }

    public void setMastersHidden(Boolean mastersHidden) {
        isMastersHidden = mastersHidden;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Object getTodayReservationsCount() {
        return todayReservationsCount;
    }

    public void setTodayReservationsCount(Object todayReservationsCount) {
        this.todayReservationsCount = todayReservationsCount;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    @Override
    public String toString() {
        return "SalonDetails{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", workStartTime='" + workStartTime + '\'' +
                ", workEndTime='" + workEndTime + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", checkRating=" + checkRating +
                ", instagramProfile='" + instagramProfile + '\'' +
                ", isMastersHidden=" + isMastersHidden +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", reviewCount=" + reviewCount +
                ", averageRating=" + averageRating +
                ", phoneNumbers=" + phoneNumbers +
                ", todayReservationsCount=" + todayReservationsCount +
                ", pictures=" + pictures +
                '}';
    }
}
