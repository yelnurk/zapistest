package com.example.zapistest.domain.repository;

import com.example.zapistest.data.model.SalonDetailsResponse;
import com.example.zapistest.domain.entity.Salon;

import java.util.List;

import io.reactivex.Single;

public interface SalonRepository {
    Single<List<Salon>> getPopular();

    Single<List<Salon>> getRecommended();

    Single<List<Salon>> getRecentlyAdded();

    Single<SalonDetailsResponse> getSalonDetails(int id);
}
