package com.example.zapistest.ui.main;

import android.os.Bundle;
import android.util.Pair;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.zapistest.R;
import com.example.zapistest.ui.home.HomeFragment;
import com.example.zapistest.ui.profile.ProfileFragment;
import com.example.zapistest.ui.search.SearchFragment;
import com.example.zapistest.ui.utils.backstack.BackStackActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;

public class MainActivity extends BackStackActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener {
    private static final String STATE_CURRENT_TAB_ID = "state_current_tab_id";
    private static final int MAIN_TAB_ID = R.id.menu_home;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private Fragment curFragment;
    private int currentMenuItemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            navigation.setSelectedItemId(MAIN_TAB_ID);
            onNavigationItemSelected(navigation.getMenu().getItem(0));
        }

        initViews();
    }

    private void initViews() {
        navigation.setOnNavigationItemSelectedListener(this);
        navigation.setOnNavigationItemReselectedListener(this);
    }

    @NonNull
    private Fragment rootTabFragment(int menuItemId) {
        switch (menuItemId) {
            case R.id.menu_home:
                return HomeFragment.newInstance();
            case R.id.menu_search:
                return SearchFragment.newInstance();
            case R.id.menu_profile:
                return ProfileFragment.newInstance();
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        curFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        currentMenuItemId = savedInstanceState.getInt(STATE_CURRENT_TAB_ID);
        navigation.setSelectedItemId(currentMenuItemId);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_TAB_ID, currentMenuItemId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        Pair<Integer, Fragment> pair = popFragmentFromBackStack();
        if (pair != null) {
            backTo(pair.first, pair.second);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (curFragment != null) {
            pushFragmentToBackStack(currentMenuItemId, curFragment);
        }
        currentMenuItemId = item.getItemId();
        Fragment fragment = popFragmentFromBackStack(currentMenuItemId);
        if (fragment == null) {
            fragment = rootTabFragment(currentMenuItemId);
        }
        replaceFragment(fragment);
        return true;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
        backToRoot();
    }

    public void showFragment(@NonNull Fragment fragment) {
        showFragment(fragment, true);
    }

    public void showFragment(@NonNull Fragment fragment, boolean addToBackStack) {
        if (curFragment != null && addToBackStack) {
            pushFragmentToBackStack(currentMenuItemId, curFragment);
        }
        replaceFragment(fragment);
    }

    private void backTo(int tabId, @NonNull Fragment fragment) {
        if (tabId != currentMenuItemId) {
            currentMenuItemId = tabId;
            navigation.setSelectedItemId(currentMenuItemId);
        }
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }

    private void backToRoot() {
        if (isRootTabFragment(curFragment, currentMenuItemId)) {
            return;
        }
        resetBackStackToRoot(currentMenuItemId);
        Fragment rootFragment = popFragmentFromBackStack(currentMenuItemId);
        assert rootFragment != null;
        backTo(currentMenuItemId, rootFragment);
    }

    private boolean isRootTabFragment(@NonNull Fragment fragment, int tabId) {
        return fragment.getClass() == rootTabFragment(tabId).getClass();
    }

    private void replaceFragment(@NonNull Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.container, fragment);
        tr.commitAllowingStateLoss();
        curFragment = fragment;
    }
}
