package com.example.zapistest.ui.details;

import android.view.View;
import android.widget.TextView;

import com.example.zapistest.R;
import com.example.zapistest.ui.base.BaseViewHolder;

import butterknife.BindView;

class DayViewHolder extends BaseViewHolder {
    @BindView(R.id.tv_week_day)
    TextView tv_week_day;
    @BindView(R.id.tv_time)
    TextView tv_time;

    DayViewHolder(View itemView) {
        super(itemView);
    }

    void onBind(String weekDay) {
        if (weekDay != null) {
            tv_week_day.setText(weekDay);
            tv_time.setText("asd");
        }
    }
}
