package com.example.zapistest.ui.utils;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.example.zapistest.utils.ContextUtils;

import org.jetbrains.annotations.NotNull;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int widthInPx;

    public SpaceItemDecoration(Context context, int widthInDp) {
        this.widthInPx = ContextUtils.dp2pixels(context, widthInDp);
    }

    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view,
                               @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        if (parent.getAdapter() != null &&
                parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
            outRect.right = widthInPx;
        }
    }
}