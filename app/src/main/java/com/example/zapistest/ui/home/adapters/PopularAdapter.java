package com.example.zapistest.ui.home.adapters;

import androidx.lifecycle.LifecycleOwner;

import com.example.zapistest.presentation.model.HomeViewModel;

public class PopularAdapter extends SalonAdapter {
    public PopularAdapter(HomeViewModel homeViewModel, LifecycleOwner lifecycleOwner) {
        homeViewModel.getPopular().observe(lifecycleOwner, salons -> {
            salonList.clear();
            if (salons != null) {
                salonList.addAll(salons);
                notifyDataSetChanged();
            }
        });
    }
}
