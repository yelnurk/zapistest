package com.example.zapistest.ui.home.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.zapistest.R;
import com.example.zapistest.data.ApiConstants;
import com.example.zapistest.domain.entity.Salon;
import com.example.zapistest.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public abstract class SalonAdapter extends RecyclerView.Adapter<SalonAdapter.SalonViewHolder> {
    final List<Salon> salonList = new ArrayList<>();
    Callback callback;

    @NonNull
    @Override
    public SalonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SalonViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_salon, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SalonViewHolder holder, int position) {
        holder.onBind(salonList.get(position));
    }

    @Override
    public int getItemCount() {
        return salonList.size();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    class SalonViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_type)
        TextView tv_type;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_rating)
        TextView tv_rating;
        @BindView(R.id.iv_picture)
        ImageView iv_picture;

        private Salon salon;

        SalonViewHolder(View itemView) {
            super(itemView);
        }

        void onBind(Salon salon) {
            if (salon != null) {
                this.salon = salon;
                tv_type.setText(salon.getType());
                tv_name.setText(salon.getName());
                tv_rating.setText(String.valueOf(salon.getCheckRating()));

                Glide
                        .with(itemView.getContext())
                        .load(ApiConstants.HOST_URL + "/" + salon.getPictureUrl())
                        .centerCrop()
                        .into(iv_picture);
            }
        }

        @OnClick(R.id.content)
        void OnClickItem() {
            if (callback != null) {
                callback.onItemClick(salon.getId());
            }
        }
    }

    public interface Callback {
        void onItemClick(int id);
    }
}
