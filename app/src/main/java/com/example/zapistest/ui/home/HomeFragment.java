package com.example.zapistest.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.zapistest.R;
import com.example.zapistest.domain.entity.Salon;
import com.example.zapistest.presentation.ViewModelFactory;
import com.example.zapistest.presentation.model.HomeViewModel;
import com.example.zapistest.ui.base.BaseFragment;
import com.example.zapistest.ui.details.DetailsActivity;
import com.example.zapistest.ui.home.adapters.PopularAdapter;
import com.example.zapistest.ui.home.adapters.RecentlyAddedAdapter;
import com.example.zapistest.ui.home.adapters.RecommendedAdapter;
import com.example.zapistest.ui.home.adapters.SalonAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class HomeFragment extends BaseFragment implements SalonAdapter.Callback {
    private static final String STATE_SCROLL_VIEW = "state_scroll_view";

    @BindView(R.id.parent)
    ScrollView parent;
    @BindView(R.id.tv_recommended)
    TextView tv_recommended;
    @BindView(R.id.tv_popular)
    TextView tv_popular;
    @BindView(R.id.tv_recently_added)
    TextView tv_recently_added;

    @BindView(R.id.rv_recommended)
    RecyclerView rv_recommended;
    @BindView(R.id.rv_popular)
    RecyclerView rv_popular;
    @BindView(R.id.rv_recently_added)
    RecyclerView rv_recently_added;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    ViewModelFactory viewModelFactory;
    private HomeViewModel viewModel;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);

        initViews();

        observableViewModel();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) return;

        final int[] position = savedInstanceState.getIntArray(STATE_SCROLL_VIEW);
        if (position != null)
            parent.post(() -> parent.scrollTo(position[0], position[1]));
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray(STATE_SCROLL_VIEW,
                new int[]{parent.getScrollX(), parent.getScrollY()});
    }

    private void initViews() {
        configureRecyclerView(rv_recommended);
        configureRecyclerView(rv_popular);
        configureRecyclerView(rv_recently_added);

        SalonAdapter recommendedAdapter = new RecommendedAdapter(viewModel, this);
        recommendedAdapter.setCallback(this);
        SalonAdapter popularAdapter = new PopularAdapter(viewModel, this);
        popularAdapter.setCallback(this);
        SalonAdapter recentlyAddedAdapter = new RecentlyAddedAdapter(viewModel, this);
        recentlyAddedAdapter.setCallback(this);

        rv_recommended.setAdapter(recommendedAdapter);
        rv_popular.setAdapter(popularAdapter);
        rv_recently_added.setAdapter(recentlyAddedAdapter);
    }

    private void observableViewModel() {
        viewModel.getRecommended().observe(this, salons -> {
            Timber.d("Home recommended %s", salons);
            onListIsEmpty(salons, tv_recommended, rv_recommended);
        });
        viewModel.getPopular().observe(this, salons -> {
            Timber.d("Home popular %s", salons);
            onListIsEmpty(salons, tv_popular, rv_popular);
        });
        viewModel.getRecentlyAdded().observe(this, salons -> {
            Timber.d("Home recently added %s", salons);
            onListIsEmpty(salons, tv_recently_added, rv_recently_added);
        });
        viewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void onListIsEmpty(List<Salon> salonList, TextView textView,
                               RecyclerView recyclerView) {
        if (salonList == null || salonList.isEmpty()) {
            textView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void configureRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL,
                false));
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
//        recyclerView.addItemDecoration(new SpaceItemDecoration(getContext(), 16));
    }

    @Override
    public void onItemClick(int id) {
        startActivity(DetailsActivity.newIntent(getContext(), id));
    }
}
