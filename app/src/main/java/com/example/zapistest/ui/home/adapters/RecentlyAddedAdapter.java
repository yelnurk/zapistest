package com.example.zapistest.ui.home.adapters;

import androidx.lifecycle.LifecycleOwner;

import com.example.zapistest.presentation.model.HomeViewModel;

public class RecentlyAddedAdapter extends SalonAdapter {
    public RecentlyAddedAdapter(HomeViewModel homeViewModel, LifecycleOwner lifecycleOwner) {
        homeViewModel.getRecentlyAdded().observe(lifecycleOwner, salons -> {
            salonList.clear();
            if (salons != null) {
                salonList.addAll(salons);
                notifyDataSetChanged();
            }
        });
    }
}
