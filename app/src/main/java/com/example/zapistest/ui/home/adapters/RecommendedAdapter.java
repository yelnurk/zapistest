package com.example.zapistest.ui.home.adapters;

import androidx.lifecycle.LifecycleOwner;

import com.example.zapistest.presentation.model.HomeViewModel;

public class RecommendedAdapter extends SalonAdapter {
    public RecommendedAdapter(HomeViewModel homeViewModel, LifecycleOwner lifecycleOwner) {
        homeViewModel.getRecommended().observe(lifecycleOwner, salons -> {
            salonList.clear();
            if (salons != null) {
                salonList.addAll(salons);
                notifyDataSetChanged();
            }
        });
    }
}
