package com.example.zapistest.ui.details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zapistest.R;
import com.example.zapistest.domain.entity.SalonDetails;
import com.example.zapistest.presentation.ViewModelFactory;
import com.example.zapistest.presentation.model.DetailsViewModel;
import com.example.zapistest.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;

public class DetailsActivity extends BaseActivity {
    private static final String EXTRA_SALON_ID = "extra_salon_id";

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_type)
    TextView tv_type;
    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.rv_days)
    RecyclerView rv_days;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    ViewModelFactory viewModelFactory;
    private DetailsViewModel viewModel;

    private DayAdapter dayAdapter;
    private int extraSalonId;

    private SalonDetails salonDetails;

    public static Intent newIntent(Context packageContext, int salonId) {
        return new Intent(packageContext, DetailsActivity.class)
                .putExtra(EXTRA_SALON_ID, salonId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(DetailsViewModel.class);

        extraSalonId = getIntent().getIntExtra(EXTRA_SALON_ID, -1);

        initViews();

        observableViewModel();
    }

    private void initViews() {
        rv_days.setLayoutManager(new LinearLayoutManager(this));
        dayAdapter = new DayAdapter();
        dayAdapter.setToday(5);
        rv_days.setAdapter(dayAdapter);
    }

    private void observableViewModel() {
        if (extraSalonId > 0) {
            viewModel.fetchDetails(extraSalonId);
        }
        viewModel.getSalonDetails().observe(this, salonDetails -> {
            this.salonDetails = salonDetails;
            updateDetails();
        });
        viewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void updateDetails() {
        if (salonDetails != null) {
            tv_name.setText(salonDetails.getName());
            tv_type.setText(salonDetails.getType());
            tv_address.setText(salonDetails.getAddress());
        }
    }
}
