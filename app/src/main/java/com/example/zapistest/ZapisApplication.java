package com.example.zapistest;

import com.example.zapistest.di.component.ApplicationComponent;
import com.example.zapistest.di.component.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

public class ZapisApplication extends DaggerApplication {
    private static ZapisApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        setupTimber();
    }

    public static ZapisApplication getApplication() {
        return application;
    }

    private void setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this)
                .build();
        component.inject(this);
        return component;
    }
}
